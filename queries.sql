//drop all tables
drop table PRODUCT;
drop table if exists PRODUCT_CATEGORY_PROPERTY ;
drop table if exists PRODUCT_CATEGORY ;
drop table if exists PRODUCT_CATEGORY_SPEC_ATTR  ;
drop table if exists PRODUCT_CATEGORY_SPEC ;
drop table if exists LOOKUP_VALUES ;
drop table if exists LOOKUP;



//Check category specifications and applicable attributes with allowed lookup values
select
PRODUCT_CATEGORY_NAME,
GROUP_CONCAT(LOOKUP_NAME||' [ '||allowed_values||' ]' ORDER BY lookup_ID SEPARATOR STRINGDECODE('\n')) as known_properties
from
(
SELECT
spec.PRODUCT_CATEGORY_NAME,
l.LOOKUP_NAME, l.ID as lookup_ID,
GROUP_CONCAT (v.LOOKUP_VALUE ORDER BY v.LOOKUP_VALUE SEPARATOR ', ') as allowed_values
 FROM
 PRODUCT_CATEGORY_SPEC spec
join
PRODUCT_CATEGORY_SPEC_ATTR attr
on (attr.PRODUCT_CATEGORY_SPEC_ID=spec.id)
join
LOOKUP l
on (attr.lookup_id=l.id)
join
LOOKUP_VALUES v
on (l.id=v.lookup_id)
group by spec.PRODUCT_CATEGORY_NAME, l.LOOKUP_NAME
order by 
spec.ID,
l.ID
)
group by PRODUCT_CATEGORY_NAME
order by PRODUCT_CATEGORY_NAME

//Check categories created depending on number of available attributes
select
pc.id as category_id,
spec.PRODUCT_CATEGORY_NAME, 
GROUP_CONCAT ( l.LOOKUP_NAME||' : '||lv.LOOKUP_VALUE ORDER BY l.LOOKUP_NAME  SEPARATOR '; ') as properties
from
       PRODUCT_CATEGORY pc
join PRODUCT_CATEGORY_SPEC spec on (pc.CATEGORY_SPECIFICATION_ID=spec.ID)
left join PRODUCT_CATEGORY_PROPERTY pcp on (pc.id=pcp.PRODUCT_CATEGORY_ID)
left join LOOKUP_VALUES lv on (pcp.LOOKUP_VALUE_ID =lv.ID)
left join LOOKUP l on (lv.LOOKUP_ID=l.ID)
group by pc.id, spec.PRODUCT_CATEGORY_NAME
order by pc.id, spec.PRODUCT_CATEGORY_NAME