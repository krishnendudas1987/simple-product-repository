package kd;

import java.util.LinkedHashMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import kd.productrepository.entity.Product;
import kd.productrepository.entity.lookup.LookupTypeValue;
import kd.productrepository.entity.productcategory.ProductCategory;
import kd.productrepository.entity.productcategory.ProductCategoryProperty;
import kd.productrepository.entity.productcategory.specification.ProductCategorySpecification;
import kd.productrepository.entity.productcategory.specification.ProductCategorySpecificationAttribute;
import kd.productrepository.exception.InvalidInputException;
import kd.productrepository.jparepo.LookupTypeValueRepository;
import kd.productrepository.jparepo.ProductCategorySpecificationAttributeRepository;
import kd.productrepository.jparepo.ProductCategorySpecificationRepository;

@Component
public class InputLineParser {
	
	private static final Logger LOG = LoggerFactory.getLogger(InputLineParser.class);
	
	@Autowired
	private ProductCategorySpecificationRepository specRepo;
	
	@Autowired
	private ProductCategorySpecificationAttributeRepository specAttrRepo;
	
	@Autowired
	private LookupTypeValueRepository lookupValueRepository;
	
	@Value("${case-sensitive}")
	private boolean caseSensitive;
	
	public Product parseLine(String line) throws InvalidInputException {
		String str[] = line.trim().split(";");
		
		String category = str[0].trim();//StringUtils.capitalize(StringUtils.lowerCase(str[0].trim()));
		LOG.debug("Category : "+category);
		ProductCategorySpecification productCategorySpecification;
		if(!caseSensitive) {
			productCategorySpecification = specRepo.findByCategoryNameIgnoreCase(category);
		} else {
			productCategorySpecification = specRepo.findByCategoryName(category);
		}
		
		if(productCategorySpecification==null) {			
			throw new InvalidInputException("'"+category+"' cannot be mapped to any known Product Category!");
		} else {
			LOG.debug("Will be mapped to ProductCategorySpecification : "+productCategorySpecification);	
		}
		
		
		int quantity = 0;
		try {
			LOG.debug("Quantity : "+str[1].trim());
			quantity = Integer.parseInt(str[1].trim());
		} catch (NumberFormatException e) {
			throw new InvalidInputException("Quantity specified is not in a valid number format!");
		}
		
		
		ProductCategory productCategory = new ProductCategory(productCategorySpecification);		
		LinkedHashMap<String, String> inputParams = new LinkedHashMap<String, String>();
		for(int i=2; i<str.length; i++) {
			String key_value_pair[] = str[i].split(":");					
			inputParams.put(key_value_pair[0].trim(), key_value_pair[1].trim());
		}
		
		
		Product product = new Product();
		
		((LinkedHashMap<String, String>)inputParams.clone()).forEach((k,v)->{
			if(k.equalsIgnoreCase("series_number")) {
				LOG.debug("Series Number : "+v);
				product.setSeriesNumber(v);
				inputParams.remove(k);
			}
			if(k.equalsIgnoreCase("manufacturer")) {
				LOG.debug("Manufacturer : "+v);
				product.setManufacturer(v);
				inputParams.remove(k);
			}
			if(k.equalsIgnoreCase("price")) {
				try {
					double price = Double.parseDouble(v);
					LOG.debug("Price : "+price);
					product.setPrice(Double.parseDouble(v));
					inputParams.remove(k);
				} catch (NumberFormatException e) {
					throw new InvalidInputException("Price specified is not in a valid decimal format!");
				}
			}
		});

		if(StringUtils.isEmpty(product.getManufacturer()) || StringUtils.isEmpty(product.getSeriesNumber()) || product.getPrice()<=0) {
			throw new InvalidInputException("One of the mandatory properties missing for Product. All of the properties [seriesNumber, manufacturer, price] must be set!");
        }
				
		LOG.debug("Parsing rest of the properties to categorize into new/existing Product Category");
		int count = 0;
		
		for(String key : inputParams.keySet()) {
			LOG.debug("\t"+key+" : "+inputParams.get(key));
			
			ProductCategorySpecificationAttribute attr = null;
			if(!caseSensitive) {
				attr = specAttrRepo.findByProductCategorySpecificationCategoryNameAndLookupNameIgnoreCase(productCategorySpecification.getCategoryName(), key);
			} else {
				attr = specAttrRepo.findByProductCategorySpecificationCategoryNameAndLookupName(productCategorySpecification.getCategoryName(), key);
			}
			
			
			if(attr==null) { 
				throw new InvalidInputException("Product Category '"+productCategorySpecification.getCategoryName()+"' do not have any known property '"+key+"'!");
			} else { 
				LOG.debug("\t\t"+attr);
				
				LookupTypeValue lookUpValue = null;
				if(!caseSensitive) {
					lookUpValue = lookupValueRepository.findByLookupNameIgnoreCaseAndValueIgnoreCase(key, inputParams.get(key));
				} else {
					lookUpValue = lookupValueRepository.findByLookupNameAndValue(key, inputParams.get(key));
				}
				
				if(lookUpValue==null) {
					throw new InvalidInputException("'"+inputParams.get(key)+"' is not within allowed list of values for Product Category '"+productCategorySpecification.getCategoryName()+"' known property '"+key+"'!");					
				} else {
					
					ProductCategoryProperty productCategoryProperty = new ProductCategoryProperty(productCategory, lookUpValue);
					
					productCategory.getProductCategoryProperty().add(productCategoryProperty);			
					
					count++;
					
				}
			}
		}
		
		if(count>=1) {
			LOG.debug("Parsing successful. Input Data is valid.");
		} else {
			throw new InvalidInputException("At least one property should be mentioned from the allowed list of properties for Product Category '"+category+"'!");
		}
		
		product.setCategory(productCategory);
		product.getStock().setAmount(quantity);
		
		
		return product;
	}
	
}
