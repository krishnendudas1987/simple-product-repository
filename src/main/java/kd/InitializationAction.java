package kd;

import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import kd.productrepository.entity.lookup.LookupType;
import kd.productrepository.entity.lookup.LookupTypeValue;
import kd.productrepository.entity.productcategory.specification.ProductCategorySpecification;
import kd.productrepository.entity.productcategory.specification.ProductCategorySpecificationAttribute;
import kd.productrepository.jparepo.LookupTypeRepository;
import kd.productrepository.jparepo.LookupTypeValueRepository;
import kd.productrepository.jparepo.ProductCategorySpecificationAttributeRepository;
import kd.productrepository.jparepo.ProductCategorySpecificationRepository;

@Component
public class InitializationAction implements ApplicationListener<ContextRefreshedEvent> {
	
	private static final Logger LOG = LoggerFactory.getLogger(InitializationAction.class);
	
	@Value("classpath:lookup-data.json")
	private Resource resource;
	
	
	
	@Value("classpath:lookup-data.json")
	private String file;
	
	@Value("${initialize-lookup}")
	private boolean init;
	
	@Autowired
	private LookupTypeRepository lookupTypeRepository;
	
	@Autowired
	private LookupTypeValueRepository lookupTypeValueRepository;
	
	@Autowired
	private ProductCategorySpecificationRepository productCategorySpecificationRepository;
	
	@Autowired
	private ProductCategorySpecificationAttributeRepository productCategorySpecificationAttributeRepository;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {		
		try {			
			//File file = new ClassPathResource("lookup-data.json").getFile();		
			//FileInputStream fIn = new FileInputStream(file);
			//InputStream inputStream = resource.getInputStream();
			/*byte[] contents = new byte[(int)file.length()];		
			fIn.read(contents);		
			fIn.close();*/
			
			if(!init) return;
			
			//String json = new String(Files.readAllBytes(resource.getFile().toPath()));;
			
			String json = IOUtils.toString(resource.getInputStream());
			
			//String json = new String(contents,"UTF-8");
			ObjectMapper objectMapper = new ObjectMapper();		
			JsonNode jsonNode = objectMapper.readTree(json);		
			
			LOG.debug("=====Initializing Lookup-values and definition of Product Category specific properties=====");
			jsonNode.forEach( (root) -> {			
				root.forEach( (node) -> {				
					String category = node.get("product-category-name").asText();
					
					
					ProductCategorySpecification catSpec = productCategorySpecificationRepository.findByCategoryName(category);	
					if(catSpec==null) {
						catSpec = new ProductCategorySpecification(category);
					}				
					
					LOG.debug(category);				
					JsonNode attr = node.get("product-category-attributes");				
					Iterator<String> it = attr.fieldNames();
					while(it.hasNext()) {					
						String attrname = it.next();
						LOG.debug("\t"+attrname);					
						LookupType lookupType = lookupTypeRepository.findByName(attrname);					
						if(lookupType==null) {
							lookupType = new LookupType(attrname);
						}					
						final LookupType lookupType_ = lookupType;					
						//lookupType = );					
						attr.get(attrname).forEach(
								(n) -> {
									LOG.debug("\t\t"+n.asText());
									
									if(lookupTypeValueRepository.findByLookupNameAndValue(attrname, n.asText())!=null) {
										LOG.debug("\t\t\tAlready exists in DB");
									} else {	
										lookupType_.getValues().add( new LookupTypeValue(lookupType_,n.asText()) );
									}
								}
						);			
						//LOG.debug(lookupType_.getId());
						lookupTypeRepository.save(lookupType_);
						//LOG.debug(lookupType_.getId());
						
						ProductCategorySpecificationAttribute catSpecAttr = productCategorySpecificationAttributeRepository.findByProductCategorySpecificationCategoryNameAndLookupName(category, attrname);
						
						if(catSpecAttr==null) {
							catSpecAttr = new ProductCategorySpecificationAttribute(lookupType_, catSpec);
							catSpec.getAttributeList().add(catSpecAttr);
						}
								
					}
					LOG.debug(catSpec.toString());
					productCategorySpecificationRepository.save(catSpec);
				});			
			});
			
			
			/*productCategorySpecificationRepository.findAll().forEach(					
					(spec) -> {
						LOG.debug("-----"+spec);
						LOG.debug("-----"+spec.hashCode());
						spec.getAttributeList().forEach(
								(attr) -> {
									LOG.debug("----------"+attr);
									LOG.debug("----------"+attr.hashCode());
								}							
						);
						
					}				
			);*/
		} catch (IOException e) {
			e.printStackTrace();
			LOG.error("Cannot locate lookup-data.json for setting up lookup-values/Product Category specific properties : can't continue!");
			System.exit( SpringApplication.exit(event.getApplicationContext(), () -> 0 ));
			
		}
		
	}
	
}