package kd.productrepository.jparepo;

import org.springframework.data.repository.CrudRepository;

import kd.productrepository.entity.lookup.LookupType;
import kd.productrepository.entity.lookup.LookupTypeValue;

public interface LookupTypeValueRepository extends CrudRepository<LookupTypeValue, Integer> {

	
	LookupTypeValue findByLookupNameAndValue(String lookupName, String value);
	
	LookupTypeValue findByLookupNameIgnoreCaseAndValueIgnoreCase(String lookupName, String value);
	
	
}
