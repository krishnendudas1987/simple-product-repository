package kd.productrepository.jparepo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import kd.productrepository.entity.Product;
import kd.productrepository.entity.productcategory.ProductCategory;

public interface ProductRepository extends CrudRepository<Product, Integer> {
	Product findById(int id);
	
	List<Product> findByCategory(ProductCategory category);
	
	List<Product> findByCategorySpecificationCategoryName(String categoryname);
	
	
	List<Product> findAllByOrderByCategorySpecificationCategoryNameAsc();
}
