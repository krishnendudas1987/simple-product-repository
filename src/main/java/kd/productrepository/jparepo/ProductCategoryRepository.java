package kd.productrepository.jparepo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import kd.productrepository.entity.productcategory.ProductCategory;
import kd.productrepository.entity.productcategory.specification.ProductCategorySpecification;

public interface ProductCategoryRepository extends CrudRepository<ProductCategory, Integer> {
	ProductCategory findById(int id);
	
	List<ProductCategory> findBySpecification(ProductCategorySpecification spec);
	
	
}
