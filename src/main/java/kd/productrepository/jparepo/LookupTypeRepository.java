package kd.productrepository.jparepo;

import org.springframework.data.repository.CrudRepository;

import kd.productrepository.entity.lookup.LookupType;

public interface LookupTypeRepository extends CrudRepository<LookupType, Integer> {

	
	LookupType findByName(String name);
	
	int deleteByName(String name);
}
