package kd.productrepository.jparepo;

import org.springframework.data.repository.CrudRepository;

import kd.productrepository.entity.productcategory.specification.ProductCategorySpecification;

public interface ProductCategorySpecificationRepository extends CrudRepository<ProductCategorySpecification, Integer>{

	public ProductCategorySpecification findByCategoryName(String categoryName);
	
	public ProductCategorySpecification findByCategoryNameIgnoreCase(String categoryName);
	
}
