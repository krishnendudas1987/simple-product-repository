package kd.productrepository.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import kd.productrepository.entity.Product;
import kd.productrepository.entity.productcategory.ProductCategory;
import kd.productrepository.jparepo.ProductRepository;

@Component
public class ProductService {
	
	private static final Logger LOG = LoggerFactory.getLogger(ProductService.class);
	
	
	@Autowired
	private ProductRepository repository;
	
	@Autowired
	private ProductCategoryService productCategoryService;
	
	
	public List<Product> getProductList() {
		return repository.findAllByOrderByCategorySpecificationCategoryNameAsc();
	}
		
	
	
	@Transactional(propagation=Propagation.REQUIRED)
	public Product createOrUpdateProduct(Product product) {		
		List<Product> list = repository.findByCategorySpecificationCategoryName(product.getCategory().getSpecification().getCategoryName());
		int index = list.indexOf(product);
				
		if(index<0) {
			ProductCategory productCategory = productCategoryService.createOrUpdateProductCategory(product.getCategory());
			product.setCategory(productCategory);			
			Product p = repository.save(product);
			LOG.info("createOrUpdateProduct : New Product created : "+p );
			return p ;	
		} else {
			Product p = list.get(index);
			LOG.info("createOrUpdateProduct : Existing Product found : "+p );
			LOG.info("Increasing stock size");
			p.getStock().setAmount( p.getStock().getAmount() +  product.getStock().getAmount() );
			if(product.getStock().getAmount()>0) {
				repository.save(p);
			}
			return p;	
		}
		
	}
	
	
}
