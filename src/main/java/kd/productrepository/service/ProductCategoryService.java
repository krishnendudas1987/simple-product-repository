package kd.productrepository.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import kd.productrepository.entity.productcategory.ProductCategory;
import kd.productrepository.jparepo.ProductCategoryRepository;

@Component
public class ProductCategoryService {
	
	private static final Logger LOG = LoggerFactory.getLogger(ProductCategoryService.class);
	
	
	@Autowired
	private ProductCategoryRepository repository;
	
	@Transactional
	public ProductCategory createOrUpdateProductCategory(ProductCategory productCategory) {
		
		List<ProductCategory> list = repository.findBySpecification(productCategory.getSpecification());
		if(!list.contains(productCategory)) {
			ProductCategory pc = repository.save(productCategory);
			LOG.info("createOrUpdateProductCategory : New ProductCategory created : "+pc);
			return pc;	
		} else {
			ProductCategory pc = list.get(list.indexOf(productCategory));
			LOG.info("createOrUpdateProductCategory : Existing ProductCategory found : "+pc);
			return pc;	
		}
		
	}
	

}
