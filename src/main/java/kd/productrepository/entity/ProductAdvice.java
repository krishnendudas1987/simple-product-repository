package kd.productrepository.entity;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import kd.productrepository.exception.CategoryPropertyAssignmentException;
import kd.productrepository.exception.ProductPropertyAssignmentException;

@Aspect
@Component
public class ProductAdvice {
	
	@Before("execution(* kd.productrepository.jparepo.ProductRepository.save(*))")
	public void before(JoinPoint joinPoint) throws CategoryPropertyAssignmentException {
        Product p = (Product)(joinPoint.getArgs()[0]);
        String errormessage = null;
        if(p.getManufacturer()==null || p.getSeriesNumber()==null || p.getPrice()<=0) {
        	errormessage = "One of the mandatory properties missing for Product. All of the properties [seriesNumber, manufacturer, price] must be set!";
        }
        else if(p.getCategory()==null) {
        	errormessage = "Product cannot be mapped to any existing product categories!";
        }
        
        if(errormessage!=null) {
        	throw new ProductPropertyAssignmentException(errormessage);
        }
        
    }
	

}
