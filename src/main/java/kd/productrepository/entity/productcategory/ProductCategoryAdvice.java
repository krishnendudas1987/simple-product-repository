package kd.productrepository.entity.productcategory;

import java.util.stream.Collectors;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import kd.productrepository.entity.lookup.LookupType;
import kd.productrepository.exception.CategoryPropertyAssignmentException;

@Aspect
@Component
public class ProductCategoryAdvice {
	
	
	@Before("execution(* kd.productrepository.jparepo.ProductCategoryRepository.save(*))")
	public void before(JoinPoint joinPoint) throws CategoryPropertyAssignmentException {
        ProductCategory pc = (ProductCategory)(joinPoint.getArgs()[0]);
        if(pc.getProductCategoryProperty().size()==0) {
        	String str = pc.getSpecification().getAttributeList().stream().map(e->"'"+e.getLookup().getName()+"'").collect(Collectors.joining(", "));
        	String errormessage = "No properties set for Product Category '"+pc.getSpecification().getCategoryName()+"'. At least one property must be set from the known list of additional properties ["+str+"]!";
    		throw new CategoryPropertyAssignmentException(errormessage);
        }
        
        for(ProductCategoryProperty prop : pc.getProductCategoryProperty()) {
        	
        	LookupType lookup = prop.getProductCategoryProperty().getLookup();
        	boolean allowedValue = pc.getSpecification().getAttributeList().stream().anyMatch(  e -> e.getLookup().equals(lookup)  );
        	if(!allowedValue) {
        		String errormessage = "'"+prop.getProductCategoryProperty().getLookup().getName()+"' is not within the known list of properties for Product Category '"+pc.getSpecification().getCategoryName()+"'!";
        		throw new CategoryPropertyAssignmentException(errormessage);        		
        	}        	
        }
    }
}
