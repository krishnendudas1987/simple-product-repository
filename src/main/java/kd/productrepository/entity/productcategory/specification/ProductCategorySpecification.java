package kd.productrepository.entity.productcategory.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Product_Category_Spec")
public class ProductCategorySpecification {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name="Product_Category_Name")
	private String categoryName; 
	
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name="Product_Category_Spec_Id",referencedColumnName="Id")
	private List<ProductCategorySpecificationAttribute> attributeList;
	
	
	
	public ProductCategorySpecification() {		
		super();
		attributeList = new ArrayList<ProductCategorySpecificationAttribute>();
	}

	public ProductCategorySpecification(String categoryName) {
		super();		
		this.categoryName = categoryName;
		attributeList = new ArrayList<ProductCategorySpecificationAttribute>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public List<ProductCategorySpecificationAttribute> getAttributeList() {
		return attributeList;
	}

	public void setAttributeList(List<ProductCategorySpecificationAttribute> attributeList) {
		this.attributeList = attributeList;
	}

	@Override
	public String toString() {
		return "ProductCategorySpecification [id=" + id + ", categoryName=" + categoryName + ", attributeList="
				+ attributeList + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		//result = prime * result + ((attributeList == null) ? 0 : attributeList.hashCode());
		result = prime * result + ((getCategoryName() == null) ? 0 : getCategoryName().hashCode());
		result = prime * result + getId();
		
		if(getAttributeList()!=null) {			
			for(ProductCategorySpecificationAttribute attr : getAttributeList()) {
				result = result + attr.hashCode();
			}
		}
		return result;
	}

	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ProductCategorySpecification))
			return false;
		
		
		ProductCategorySpecification other = (ProductCategorySpecification) obj;
		
		if (getCategoryName() == null) {
			if (other.getCategoryName() != null)
				return false;
		} else if (!getCategoryName().equals(other.getCategoryName()))
			return false;
		if (getId() != other.getId())
			return false;
		
		if (getAttributeList() == null) {
			if (other.getAttributeList() != null)
				return false;
		}
		if(other.getAttributeList() == null) {
			if (getAttributeList() != null)
				return false;
		}
		
		//else if (!getAttributeList().equals(other.getAttributeList()))
		//	return false;
		
		else {
			List<ProductCategorySpecificationAttribute> list =new ArrayList<ProductCategorySpecificationAttribute>();
			list.addAll(getAttributeList());
			
			for(ProductCategorySpecificationAttribute attr : other.getAttributeList()) {
				list.remove(attr);
			}
			if(list.size()>0) {
				return false;
			}
		}
		
		return true;
	}
	
	
	
	

}
