package kd.productrepository.entity.productcategory.specification;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import kd.productrepository.entity.lookup.LookupType;


@Entity
@Table(name="Product_Category_Spec_Attr")
public class ProductCategorySpecificationAttribute {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="Lookup_Id", nullable=false, foreignKey=@ForeignKey(name="Fk_PCSA_Lookup_Id"))
	private LookupType lookup;
	
	@ManyToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name="Product_Category_Spec_Id", nullable=false, foreignKey=@ForeignKey(name="Fk_Product_Category_Spec_Id"))
	@OnDelete(action = OnDeleteAction.CASCADE)
	private ProductCategorySpecification productCategorySpecification;

	
	public ProductCategorySpecificationAttribute( ) {
		super();
	}
	
	public ProductCategorySpecificationAttribute(LookupType lookup,
			ProductCategorySpecification productCategorySpecification) {
		super();
		this.lookup = lookup;
		this.productCategorySpecification = productCategorySpecification;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LookupType getLookup() {
		return lookup;
	}

	public void setLookup(LookupType lookup) {
		this.lookup = lookup;
	}

	public ProductCategorySpecification getProductCategorySpecification() {
		return productCategorySpecification;
	}

	public void setProductCategorySpecification(ProductCategorySpecification productCategorySpecification) {
		this.productCategorySpecification = productCategorySpecification;
	}

	@Override
	public String toString() {
		return "ProductCategorySpecificationAttribute [id=" + id + ", lookup=" + lookup + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getId();
		result = prime * result + ((getLookup() == null) ? 0 : getLookup().hashCode());
		result = prime * result
				+ ((getProductCategorySpecification()== null) ? 0 : getProductCategorySpecification().getId());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ProductCategorySpecificationAttribute))
			return false;
		
		
		ProductCategorySpecificationAttribute other = (ProductCategorySpecificationAttribute) obj;
		if (getId() != other.getId())
			return false;
		if (getLookup() == null) {
			if (other.getLookup() != null)
				return false;
		} else if (!getLookup().equals(other.getLookup()))
			return false;
		if (getProductCategorySpecification() == null) {
			if (other.getProductCategorySpecification() != null)
				return false;
		} else if (getProductCategorySpecification().getId()!=other.getProductCategorySpecification().getId())
			return false;
		return true;
	}
	
	
	

}
