package kd.productrepository.entity.productcategory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import kd.productrepository.entity.productcategory.specification.ProductCategorySpecification;

@Entity
@Table(name="Product_Category")
public class ProductCategory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	
	/*@Column(name="Category_Name")
	private String name;*/ 
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="Category_Specification_Id", nullable=false, foreignKey=@ForeignKey(name="Fk_Category_Specification_Id"))
	private ProductCategorySpecification specification;
	
	
	
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name="Product_Category_Id",referencedColumnName="Id")
	//@OrderBy(value="productCategoryPropertyValue")
	private List<ProductCategoryProperty> productCategoryProperty;

	
	

	@Override
	public String toString() {
		return "ProductCategory [id=" + id + ", specification=" + specification + ", productCategoryProperty=" + productCategoryProperty + "]";
	}

	public ProductCategory() {
		super();
		productCategoryProperty = new ArrayList<ProductCategoryProperty>(); 
	}
	
	public ProductCategory(ProductCategorySpecification spec) {
		super();
		this.specification = spec;
		productCategoryProperty = new ArrayList<ProductCategoryProperty>(); 
	}
	
	
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public ProductCategorySpecification getSpecification() {
		return specification;
	}


	public void setName(ProductCategorySpecification spec) {
		this.specification = spec;
	}


	public List<ProductCategoryProperty> getProductCategoryProperty() {
		return productCategoryProperty;
	}


	public void setProductCategoryProperty(List<ProductCategoryProperty> productCategoryProperty) {
		this.productCategoryProperty = productCategoryProperty;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		//result = prime * result + id;
		result = prime * result + ((getSpecification() == null) ? 0 : getSpecification().hashCode());
		//result = prime * result + ((productCategoryProperty == null) ? 0 : productCategoryProperty.hashCode());
		if(getProductCategoryProperty() != null) {
			for(ProductCategoryProperty pc : getProductCategoryProperty()) {
				result = result + pc.hashCode();
			}
		}
		
		
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if ( !(obj instanceof ProductCategory))
			return false;
		ProductCategory other = (ProductCategory) obj;
		//if (id != other.id)
		//	return false;
		if (getSpecification() == null) {
			if (other.getSpecification() != null)
				return false;
		} else if (!getSpecification().equals(other.getSpecification())) {
			return false;
		}	
		if (getProductCategoryProperty() == null) {
			if (other.getProductCategoryProperty() != null)
				return false;
		} else {
			List<ProductCategoryProperty> list =new ArrayList<ProductCategoryProperty>();
			list.addAll(getProductCategoryProperty());		
			
			List<ProductCategoryProperty> listOther =new ArrayList<ProductCategoryProperty>();
			listOther.addAll(other.getProductCategoryProperty());
			
			list.removeAll(other.getProductCategoryProperty());
			listOther.removeAll(getProductCategoryProperty());
			
			/*for(ProductCategoryProperty pc : other.getProductCategoryProperty()) {
				list.remove(pc);
			}*/
			if(list.size()>0 || listOther.size()>0) {				
				return false;
			}
			
		}
		
		return true;
	}

	
	
	
	
}
