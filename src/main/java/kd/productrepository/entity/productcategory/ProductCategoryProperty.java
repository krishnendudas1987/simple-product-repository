package kd.productrepository.entity.productcategory;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import kd.productrepository.entity.lookup.LookupTypeValue;

@Entity
@Table(name="Product_Category_Property")
public class ProductCategoryProperty {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@ManyToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name="Product_Category_Id", nullable=false, foreignKey=@ForeignKey(name="Fk_Product_Category_Id"))
	@OnDelete(action = OnDeleteAction.CASCADE)
	private ProductCategory productCategory; 

	@ManyToOne(fetch=FetchType.LAZY, cascade=CascadeType.MERGE)
	@JoinColumn(name="Lookup_Value_Id", nullable=false, foreignKey=@ForeignKey(name="Fk_LookupType_Value_Id"))
	//@OnDelete(action = OnDeleteAction.CASCADE)
	private LookupTypeValue productCategoryProperty;



	
	
	
	@Override
	public String toString() {
		return "ProductCategoryProperty [id=" + id + ", productCategoryProperty=" + productCategoryProperty + "]";
	}

	public ProductCategoryProperty() {
		super();
	}
	
	public ProductCategoryProperty(ProductCategory productCategory, LookupTypeValue productCategoryProperty) {
		super();
		this.productCategory = productCategory;
		this.productCategoryProperty = productCategoryProperty;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public ProductCategory getProductCategory() {
		return productCategory;
	}



	public void setProductCategory(ProductCategory productCategory) {
		this.productCategory = productCategory;
	}



	public LookupTypeValue getProductCategoryProperty() {
		return productCategoryProperty;
	}



	public void setProductCategoryProperty(LookupTypeValue productCategoryProperty) {
		this.productCategoryProperty = productCategoryProperty;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		//result = prime * result + id;
		//result = prime * result + ((productCategory == null) ? 0 : productCategory.hashCode());
		result = prime * result + ((productCategoryProperty == null) ? 0 : productCategoryProperty.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if ( !(obj instanceof ProductCategoryProperty))
			return false;
		ProductCategoryProperty other = (ProductCategoryProperty) obj;
		
		
		//if (id != other.id)
		//	return false;
		if (getProductCategory() == null) {
			if (other.getProductCategory() != null)
				return false;
		} 
		//else if (!productCategory.equals(other.productCategory))
		//	return false;
		if (getProductCategoryProperty() == null) {
			if (other.getProductCategoryProperty() != null)
				return false;
		} else if (!getProductCategoryProperty().equals(other.getProductCategoryProperty())) {
			//System.out.println(productCategoryProperty.equals(other.productCategoryProperty));
			//System.out.println("---Here no match");
			return false;
		}
		//System.out.println("\t\tMatch");
		return true;
	}
	

	
}
