package kd.productrepository.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import kd.productrepository.entity.productcategory.ProductCategory;

@Entity
@Table(name="Product")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name="Series_Number", nullable=false)
	private String seriesNumber;
	
	@Column(name="Manufacturer", nullable=false)	
	private String manufacturer; 	
	
	@Column(name="Price", nullable=false)
	private double price;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="Product_Stock_Id", nullable=false, foreignKey=@ForeignKey(name="Fk_Stock_Id"))
    private ProductStock stock;
	
	@ManyToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name="Product_Category_Id", nullable=false, foreignKey=@ForeignKey(name="Fk_Product_2_Product_Category_Id"))
	private ProductCategory category;
	
	
	
	

	public Product() {
		super();
		stock = new ProductStock(this, 0);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSeriesNumber() {
		return seriesNumber;
	}

	public void setSeriesNumber(String seriesNumber) {
		this.seriesNumber = seriesNumber;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	

	public ProductCategory getCategory() {
		return category;
	}

	public void setCategory(ProductCategory category) {
		this.category = category;
	}

	
	
	
	
	public ProductStock getStock() {
		return stock;
	}

	public void setStock(ProductStock stock) {
		this.stock = stock;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", seriesNumber=" + seriesNumber + ", manufacturer=" + manufacturer + ", price="
				+ price + ", stock=" + stock + ", category=" + category + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getCategory() == null) ? 0 : getCategory().hashCode());
		//result = prime * result + getId();
		result = prime * result + ((getManufacturer() == null) ? 0 : getManufacturer().hashCode());
		long temp;
		temp = Double.doubleToLongBits(getPrice());
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((getSeriesNumber() == null) ? 0 : getSeriesNumber().hashCode());
		//result = prime * result + stockSize;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if ( !(obj instanceof Product))
			return false;
		Product other = (Product) obj;
		if (getCategory() == null) {
			if (other.getCategory() != null)
				return false;
		} else if (!getCategory().equals(other.getCategory()))
			return false;
		//if (id != other.id)
		//	return false;
		if (getManufacturer() == null) {
			if (other.getManufacturer() != null)
				return false;
		} else if (!getManufacturer().equals(other.getManufacturer()))
			return false;
		if (Double.doubleToLongBits(getPrice()) != Double.doubleToLongBits(other.getPrice()))
			return false;
		if (getSeriesNumber() == null) {
			if (other.getSeriesNumber() != null)
				return false;
		} else if (!getSeriesNumber().equals(other.getSeriesNumber()))
			return false;
		//if (stockSize != other.stockSize)
		//	return false;
		return true;
	}

	
	 
	
	
}
