package kd.productrepository.entity.lookup;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;



@Entity
@Table(name="LookupValues")
public class LookupTypeValue {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	
	@Column(name="Lookup_Value")
	private String value;
	
	
	
	@ManyToOne(fetch=FetchType.LAZY, cascade=CascadeType.PERSIST)
	@JoinColumn(name="Lookup_Id", nullable=false, foreignKey=@ForeignKey(name="Fk_Lookup_Id"))
	@OnDelete(action = OnDeleteAction.CASCADE)
	private LookupType lookup; 

	
	
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getId();
		//result = prime * result + ((lookup == null) ? 0 : lookup.hashCode());
		result = prime * result + ((getLookup() == null) ? 0 : getLookup().getId());
		result = prime * result + ((getValue() == null) ? 0 : getValue().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof LookupTypeValue)) {			
			return false;
		}
		LookupTypeValue other = (LookupTypeValue) obj;
		
		if (getId() != other.getId()) {
			return false;
		}
		if (getLookup() == null) {
			if (other.getLookup() != null)
				return false;
		} else if (getLookup().getId()!=other.getLookup().getId())
			return false;
		if (getValue() == null) {
			if (other.getValue() != null)
				return false;
		} else if (!getValue().equals(other.getValue()))
			return false;
		return true;
	}

	public LookupTypeValue() {
		super();
	}

	public LookupTypeValue(LookupType lookup, String value) {
		this.value = value;
		this.lookup = lookup;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public LookupType getLookup() {
		return lookup;
	}
	public void setLookup(LookupType lookup) {
		this.lookup = lookup;
	}

	@Override
	public String toString() {
		return "LookupTypeValue [id=" + id + ", value=" + value + ", lookup=" + lookup + "]";
	}

	
	
	
	
	
}
