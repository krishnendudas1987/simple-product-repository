package kd.productrepository.entity.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NaturalId;

import kd.productrepository.entity.productcategory.ProductCategoryProperty;

@Entity
@Table(name="Lookup")
public class LookupType {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@NaturalId
	@Column(name="Lookup_Name")
	private String name;
	
	
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name="Lookup_Id",referencedColumnName="Id")
	private List<LookupTypeValue> values;
	
	
	public LookupType() {
		super();
		values = new ArrayList<LookupTypeValue>(); 
	}

	public LookupType(String name) {
		this.name = name;
		values = new ArrayList<LookupTypeValue>(); 
	}

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<LookupTypeValue> getValues() {
		return values;
	}

	public void setValues(List<LookupTypeValue> values) {
		this.values = values;
	}

	@Override
	public String toString() {
		return "LookupType [id=" + id + ", name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getId();
		result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
		//result = prime * result + ((values == null) ? 0 : values.hashCode());
		if(getValues()!=null) {			
			for(LookupTypeValue lt : getValues()) {
				result = result + lt.hashCode();
			}
		}
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof LookupType))
			return false;
		LookupType other = (LookupType) obj;
		if (getId() != other.getId())
			return false;
		if (getName() == null) {
			if (other.getName() != null)
				return false;
		} else if (!getName().equals(other.getName()))
			return false;
		if (getValues() == null) {
			if (other.getValues() != null)
				return false;
		} 
		if(other.getValues() == null) {
			if(getValues() != null) {
				return false;
			}
		}
		//else if (!values.equals(other.values))
		//	return false;
		else {
			List<LookupTypeValue> list =new ArrayList<LookupTypeValue>();
			list.addAll(getValues());
			
			List<LookupTypeValue> listOther =new ArrayList<LookupTypeValue>();
			listOther.addAll(other.getValues());
			
			list.removeAll(other.getValues());
			listOther.removeAll(getValues());
			
			
			if(list.size()>0 || listOther.size()>0) {
				return false;
			}
		}
		return true;
	}

	
	

}
