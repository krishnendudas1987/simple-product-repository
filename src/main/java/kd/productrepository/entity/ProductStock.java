package kd.productrepository.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
@Entity
@Table(name="Product_Stock")
public class ProductStock {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private int id;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)//, mappedBy = "stock")
	@JoinColumn(name="Product_Id", foreignKey=@ForeignKey(name="Fk_Product_Id"))
	private Product product;
	
	@Column(name="quantity", nullable=false)
	private int amount;
	
	public ProductStock() {
		super();
	}
	
	
	
	public ProductStock(Product product, int amount) {
		super();
		this.product = product;
		this.amount = amount;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "ProductStock [id=" + id + ", amount=" + amount + "]";
	}

}
