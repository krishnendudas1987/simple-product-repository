package kd.productrepository.exception;

public class InvalidInputException extends RuntimeException  {
	
	private static final long serialVersionUID = -2587751442988317386L;

	public InvalidInputException(String message) {
		super(message);
	}
	
	public InvalidInputException(String message, Throwable cause) {
        super(message, cause);
    }
}
