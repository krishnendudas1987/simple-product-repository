package kd.productrepository.exception;

public class ProductPropertyAssignmentException extends RuntimeException  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ProductPropertyAssignmentException(String message) {
		super(message);
	}
}
