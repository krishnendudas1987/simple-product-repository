package kd.productrepository.exception;



public class CategoryPropertyAssignmentException extends RuntimeException  {
	
	private static final long serialVersionUID = -1856297067180064886L;

	public CategoryPropertyAssignmentException(String message) {
		super(message);
	}
	
}
