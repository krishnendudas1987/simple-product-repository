package kd;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;



@SpringBootApplication
public class Application {
	
	
	private static void validateArgs(String[] args) {
		String helperMsg1 = "args[0] : import/export\nargs[1] : <file_name>";
		String helperMsg = "Invalid input format!\nargs[0] : import/export\nargs[1] : <file_name>";
		
		if(args==null||(args!=null && args.length==1 && args[0].equals("--help"))) {
			System.out.println(helperMsg1);
			System.exit(0);
		} else if(args==null||(args!=null && args.length!=2)) {
			System.err.println(helperMsg);
			System.exit(-1);
		} else if(!(args[0].equals("import")||args[0].equals("export"))) {
			System.err.println(helperMsg);
			System.exit(-1);
		} else {
			File f = new File(args[1]);			
			try {
				f.getCanonicalPath();
			} catch (IOException e) {
				System.err.println("Invalid filesystem path!");
				System.exit(-1);
			}			
			if(args[0].equals("import") && !f.exists()) {
				System.err.println("File doesn't exist!");	
				System.exit(-1);
			} else if(args[0].equals("export")) {
				//System.out.println(f.getParentFile());
				Path parent = null;
				if(f.getParentFile()==null) {
					parent = FileSystems.getDefault().getPath(".");
				} else {
					parent = f.getParentFile().toPath();
				}
				if(!Files.isWritable(parent)) {
					System.err.println("Do no have write permission! : "+f.getParent());
					System.exit(-1);
				}			
			}
		}
	}

	public static void main(String[] args) {		
		/*args = new String[] { 
			"export", "output.txt"
			//	"--help"
		};*/
			
		validateArgs(args);		
		SpringApplication.run(Application.class, args);

	}
	


	@Bean	
	public CommandLineRunner demo(CSVFileUtil csvFileUtil) {
		return (args) -> {
			if(args[0].equals("import")) {
				try {
					csvFileUtil.importFromFile(args[1]);
				} catch (Exception e) {
					e.getMessage();
				}
			} else if(args[0].equals("export")){
				try {
					csvFileUtil.exportToFile(args[1]);
				} catch (Error e) {
					e.getMessage();
				}
			}
		};
	}
}
