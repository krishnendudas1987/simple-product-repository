package kd;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import kd.productrepository.entity.Product;
import kd.productrepository.entity.lookup.LookupTypeValue;
import kd.productrepository.entity.productcategory.ProductCategoryProperty;
import kd.productrepository.exception.InvalidInputException;
import kd.productrepository.service.ProductService;

@Component
public class CSVFileUtil {
	
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private InputLineParser inputLineParser;
	
	@Value("${partial-import}")
	private boolean partialImport;
	
	private static final Logger LOG = LoggerFactory.getLogger(CSVFileUtil.class);
	
	
	public void exportToFile(String file) throws FileNotFoundException, IOException {		
		try(RandomAccessFile raf = new RandomAccessFile(file, "rw")) {
			LOG.info("Exporting to the file : "+file);
			raf.setLength(0);
			AtomicInteger counter = new AtomicInteger(0);
			productService.getProductList().forEach(
				(p) -> {
					counter.incrementAndGet();
					StringBuilder sb = new StringBuilder();
					sb.append(p.getCategory().getSpecification().getCategoryName()).append(";");
					sb.append(p.getStock().getAmount()).append(";");				
					sb.append("series_number:").append(p.getSeriesNumber()).append(";");
					sb.append("manufacturer:").append(p.getManufacturer()).append(";");
					sb.append("price:").append(p.getPrice()).append(";");		
					
					for(ProductCategoryProperty prop : p.getCategory().getProductCategoryProperty()) {
						LookupTypeValue l = prop.getProductCategoryProperty();
						sb.append(l.getLookup().getName()).append(":").append(l.getValue()).append(";");
					}				
					sb.deleteCharAt(sb.length()-1);
					LOG.debug(sb.toString());
					sb.append("\n");
					try {
						raf.write(sb.toString().getBytes());
					} catch (Exception e) {
						LOG.error(e.getMessage());
						throw new RuntimeException(e.getCause());
					}
				}
			);
			LOG.info("Total Products exported : "+counter.get());
		} catch (IOException ex) {
			throw ex;
		}
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	public void importFromFile(String file) throws FileNotFoundException, IOException, InvalidInputException {
		try(RandomAccessFile raf = new RandomAccessFile(file, "rw")) {
			String  line = null;			
			int count = 0;
			ArrayList<Integer> lines = new ArrayList<Integer>();
			try {
				LOG.info("Importing from the file : "+file);		
				while((line=raf.readLine())!=null) {		
					LOG.info("===== Input line : "+line);				
					Product product = inputLineParser.parseLine(line);				
					productService.createOrUpdateProduct(product);
					count++;
					lines.add(count);
				}
			} catch (InvalidInputException e) {
				LOG.error("***** ERROR *****"+e.getMessage());
				if(partialImport) {			
					LOG.error(">>> Skipping to next line!");								
				} else {
					LOG.error(">>> Cancelled Importing from the file : "+file);
					throw e;
				}
			}
			LOG.info("Total Products imported : "+count+" : Imported Lines from the file >> "+lines.toString());
			
		} catch (IOException ex) {
			throw ex;
		}
	}

}
